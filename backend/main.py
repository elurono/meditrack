from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
from typing import List, Optional

app = FastAPI()


# Pydantic models
class Patient(BaseModel):
    id: Optional[int]
    name: str
    dob: str
    medical_history: Optional[str]


class Appointment(BaseModel):
    id: Optional[int]
    patient_id: int
    date: str
    description: Optional[str]


# Sample in-memory data storage
patients = []
appointments = []


# Utility Functions
def get_patient_by_id(patient_id: int):
    return next(
        (patient for patient in patients if patient.id == patient_id), None)


# Patient Endpoints
@app.post("/patients", response_model=Patient)
def add_patient(patient: Patient):
    patient.id = len(patients) + 1  # Simple ID assignment
    patients.append(patient)
    return patient


@app.get("/patients", response_model=List[Patient])
def get_patients():
    return patients


@app.get("/patients/{patient_id}", response_model=Patient)
def get_patient(patient_id: int):
    patient = get_patient_by_id(patient_id)
    if patient is None:
        raise HTTPException(status_code=404, detail="Patient not found")
    return patient


@app.put("/patients/{patient_id}", response_model=Patient)
def update_patient(patient_id: int, updated_patient: Patient):
    patient = get_patient_by_id(patient_id)
    if patient is None:
        raise HTTPException(status_code=404, detail="Patient not found")
    # Update logic here
    return updated_patient  # Placeholder


@app.delete("/patients/{patient_id}")
def delete_patient(patient_id: int):
    # Deletion logic here
    return {"message": "Patient deleted successfully"}


# Appointment Endpoints
@app.post("/appointments", response_model=Appointment)
def create_appointment(appointment: Appointment):
    if get_patient_by_id(appointment.patient_id) is None:
        raise HTTPException(status_code=404, detail="Patient not found")

    appointment.id = len(appointments) + 1  # Simple ID assignment
    appointments.append(appointment)
    return appointment


@app.get("/appointments", response_model=List[Appointment])
def get_appointments():
    return appointments


@app.get("/appointments/{appointment_id}", response_model=Appointment)
def get_appointment(appointment_id: int):
    # Logic to get a specific appointment
    return {}  # Placeholder


@app.put("/appointments/{appointment_id}", response_model=Appointment)
def update_appointment(appointment_id: int, updated_appointment: Appointment):
    # Update logic here
    return updated_appointment  # Placeholder


@app.delete("/appointments/{appointment_id}")
def cancel_appointment(appointment_id: int):
    # Cancellation logic here
    return {"message": "Appointment cancelled successfully"}
